FROM ubuntu:18.04
LABEL version="1.0" description="Ubuntu_OracleCloud_OCI"
MAINTAINER "Rafael Santos"

RUN apt update && apt install -y wget curl jq
RUN cd /tmp && wget https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh && chmod 777 /tmp/install.sh && sed -i 's/apt_get_opts=""/apt_get_opts="-y"/' /tmp/install.sh

RUN /tmp/install.sh --install-dir /usr/local/lib/oracle-cli  --exec-dir /usr/local/bin \
                                                          --script-dir /usr/local/lib/oci-cli-scripts \
                                                          --update-path-and-enable-tab-completion \
                                                          --rc-file-path /etc/profile \
                                                          --optional-features db \
                                                          --no-tty
RUN useradd -m -s /bin/bash -c "OCI User account" oci
USER oci
WORKDIR /home/oci

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV PATH="$PATH:/usr/local/bin/oci"
