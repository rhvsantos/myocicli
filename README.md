# Welcome to my project called *myocicli*

So, in this project, I improved an old Shell Script that I have made, this script was usefull for me in that time, because I´ve needed to 
upload a lot of files (+1K) in different buckets and delete a lot of files also. **So this script aims only objects operations.**

Basically:
    
- I´ve created a docker image with OCI CLI (as you can see in the Dockerfile);
- I´ve made a shell script to turn the things easier, with easy setup and more intuitive (if you just want to manage your files into a bucket).
    
And the most important point, I´ve learned new things while I was doing this script. So, even if the script is not so innovative, for me 
was a good apprenticeship. For example, I´ve learned more about Bash Parameter Substitution, Arrays and also improved my programming logical. 


You can use those options below to execute the script  :

    	[Setup]
			Setup oci config.

		Example: 	$0 setup
			
		[User]
			list
				List all tenancy users
		
		Example: 	$0 user list

		[Compartment]

			create
				Create a compartment

			delete
				Delete a compartment
			
			list
				List all compartments

		Usage Example: 	$0 compartment delete <compartment>

		[Run]
			Just run Bash in OCI Container or OCI Comand directly.

		Usage Example: 	$0 run 		# Will run a bash
						$0 run oci iam user list

		[Upload]

			dir <directory> [ REQUIRED (unless file was specified)]
				Bulk-Upload a folder to a bucket.
			
			file <file>	[ REQUIRED (unless dir was specified) ]
				Upload a file to a bucket.
			
			to <Bucket Name> [ REQUIRED ]
				Specify a Bucket target.
			
			-T <Standard|InfrequentAccess|Archive>
				Select a different Tier from bucket.
			
		Usage Example: 	$0 upload dir /etc to my-bucket1 -T Archive

		[Bucket]

			create
				Create a new Bucket.
			
			delete <Bucket Name>
				Delete a specific bucket, it must be EMPTY.
			list
				List buckets

		Usage Example: 	$0 bucket delete my-bucket1

